<?php

namespace Drupal\first_module\Controller;

use Drupal\Core\Controller\ControllerBase;

class ContactController extends ControllerBase
{
  public function content(): array
  {
    $param = \Drupal::request()->query->get('q');

    if ('inscription-newsletter' === $param) {
      $form = \Drupal::formBuilder()->getForm('Drupal\first_module\Form\ContactForm');

      return $form;
    }

    $build = [
      '#markup' => 'Hello World!',
    ];
    return $build;
  }
}
