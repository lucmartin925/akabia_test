<?php

namespace Drupal\first_module\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ContactForm extends FormBase {

  public function getFormId(): string
  {
    return 'akabia_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state): array
  {
    $node = \Drupal::entityTypeManager()
      ->getStorage('node');

    $form['civility'] = [
      '#type' => 'select',
      '#title' => 'Civilité',
      '#options' => ['H' => 'H', 'F' => 'F', 'NSPP' =>'NSPP'],
      '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => 'Nom',
      '#required' => TRUE,
    ];

    $form['mail'] = [
      '#type' => 'email',
      '#title' => 'Adresse mail',
      '#required' => TRUE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;

  }

  public function validateForm(array &$form, FormStateInterface $form_state) : void
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state): void
  {
    $civility = $form_state->getValue('civility');
    $name = $form_state->getValue('name');
    $mail = $form_state->getValue('mail');

    $data = [
      'type' => 'inscription_newsletter',
      'title' => $mail,
      'field_civility' => $civility,
      'field_name' => $name,
      'field_mail' => $mail,
    ];

    $node = \Drupal::entityTypeManager()
      ->getStorage('node')
      ->create($data);
    $node->save();

    $messenger = \Drupal::messenger();
    $messenger->addStatus('Votre soumission a bien été prise en compte');

    $form_state->setRedirect('first_module.contact');

  }
}
